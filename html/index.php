<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>www.rsvp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/rsvp.css"/>
    <link rel="stylesheet" href="../css/responsive.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            

</head>
<body>
    
    <!-- follow us link -->

    <!-- end of follows links -->
    <!-- main header -->
<div class="containers">
    <video autoplay muted loop id="myVideo">
        <source src="../videos/RYVCPC VIDEO FINAL.mp4" type="video/MP4"/>
    </video>
<div class="content">
<div class="wrapper">
    <div class="navbar">
    <div class="toggle">
        <i class="fas fa-bars " id="menu"></i>
    </div>

        <ul id="nav_link">
            <li >
                <a href="#">For Organization</a>
            </li>
            
            <li>
                <a href="aboutus.php">About Us</a>
            </li>
            <li>
                <a href="structure.php">Structure</a>
            </li>
        </ul>
       
</div>

<div id="logo">
        <img src="../img/lg.jpg"/>
    </div>
</div>
</div>


    <!-- end of main header -->
  
    
<div class="runabout">
<!-- core values -->
<div class="core_value">
    <h3>Core Values </h3>
    <ul>
        Patriotism<hr>
    </ul>
    <ul>
        Honest<hr>
    </ul>
    <ul>
        Determination<hr>
    </ul>
    <ul>
        To Be Discipline<hr>
    </ul>
    <ul>
        To Be Professional
    </ul>

</div>
    <div class="description">
        <header></header>
        <p>

        </p>
        <a href="member.php"><button id="member"></button></a>

    </div>

   
</div>
</div>
</div>
</div>




<!-- mission and vision -->
<div class="all_about">
    <div class="maincontainer">
        <div class="thecard">
            <div class="thefront">
                <h3></h3>
                <div class="mission">
                    <p></p>
                </div>
                <center>
                <div class="slogan_mission">
                 <p></p>
                </div>
                </center>
                
            </div>
            <div class="theback">
                <h3></h3>
            <div class="vision">
                    <p></p>
                </div>
                <center>
                <div class="slogan_vision">
                 <p></p>
                </div>
                </center>
            </div>
        </div>
        
    </div>
 
</div>



<!-- NEWS AND UPDATES -->
<div class="all_events">
    <div class="event_back"> 
<center><h3></h3></center>
    <div class="events">
    
    <!-- this is for flex -->
   
        <div class="box1">
            <!-- <h4 id="title">Title</h4> -->
            <div class="images">
               
                <img src="../img/1.jpg"/>
            </div>
            <div class="about_event">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.,</p>
            </div>
            <button>Read More</button>
            
        </div>


        <!-- second box -->

        <div class="box1">
            <!-- <h4 id="title">Title</h4> -->
            <div class="images">
               
                <img src="../img/1.jpg"/>
            </div>
            <div class="about_event">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.,</p>
            </div>
            <button>Read More</button>
            
        </div>

        <!-- thrid box -->
        
        <div class="box1">
            <!-- <h4 id="title">Title</h4> -->
            <div class="images">
               
                <img src="../img/1.jpg"/>
            </div>
            <div class="about_event">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.,</p>
            </div>
            <button class="lesson-hover">Read More</button>  
            <p class="text-contents">An open source scripting language that can be embedded into HTML, and well suited for web development.</p> 
        </div> 
</div>
</div>
</div>

 <!-- achivement section -->

<div class="achievements">
    <div class="achievements_container">
            <div class="achieve ">
                <img src="../img/3.jpg"/>
                
                <h3><i class="fab fa-autoprefixer"></i></h3>
                <h4>Security and crime prevention related activities</h4>
                <p>Youth volunteers in partnership with police established anti-crime clubs in different schools such as GS KACYIRU II, 
                    ESPANYA, IGIHOZO SCHOOL, MARANATHA, �.</p>
                
            </div>

             <div class="achieve">
                <img src="../img/3.jpg"/>
                
                <h3><i class="fab fa-autoprefixer"></i></h3>
                <h4> Civic activities</h4>
                <p>On 23/04/2014 Youth volunteers visited Kigali Genocide memorial site at Gisozi with the aim to dignify 
                    the beloved ones we lost during 1994 genocide. </p>
                
            </div>
            <div class="achieve">
                <img src="../img/3.jpg"/>
               
                <h3><i class="fab fa-autoprefixer"></i></h3>
                <h4>Human security</h4>
                <p>RYVCP Commissioner of Training; Debriefing after community work</p>
               
            </div>
        </div>
</div>




<!-- ================================================ -->




<!-- fot test -->
    <!-- partners -->
    <center><h3 id="header"></h3></center>
<div class="partners" id="hideme">
    
    <div class="all_partners">
    <a href="#" class="box">
    <h2><span>RNP -</span> Rwanda National Police</h2>
    <!-- <img src="../img/Partner.jpg"/> -->
    
    </a>

    <a href="#" class="box">
    <h2><span>Minalok -</span> Rwanda Ministry</h2>
   
    </a>

    <a href="#" class="box">
    <h2><span>SFH -</span> Rwanda </h2>
    
    </a>

    <a href="#" class="box">
    <h2><span>Cok -</span> Rwanda </h2>
    
    </a>    
   
</div>
</div>
        <!-- partners -->
     
        <!-- contactus -->
        <div class="contact_details">
        <center><h3></h3></center>
        <div class="contact">
            
            <div class="contactinfo">
                <form>
                <label id="label">
                    FullName
                </label>
                <input type="email" name="" required>
                <label id="label">
                    E-mail
                </label><br/>
                <input type="email" name="" required>
                <label id="label">
                    Message
                </label><br/>
                <textarea id="my_subject" cols="94" rows="5" charswidth="23" name="text_body"></textarea><br/>
                <center><button id="submit">Submit</button></center>
    

                </form>
                
            </div>

        </div>
        </div>
        <!-- contat -->
        <!-- footer -->


<div class="footer">
        
        <h4>&copy; copyright RSVP 2018</h4>
<center>
<div class="subscribe">
    <div class="email">
    <input type="email" name="" placeholder="Subscribe here .........">
    </div>
    
    <div class="enevlop">
    <i class="fas fa-envelope"></i>
    </div>
    
</div>
</center>
            
        <div class="followus">
        <div class="cons">
            <i style="color:white" class="fab fa-twitter"></i>
        </div>
        <div class="cons">
            <i style="color:white" class="fab fa-facebook-f"></i>
        </div>
        <div class="cons">
            <i style="color:white"class="fab fa-youtube"></i>
        </div>
        <div class="cons">
            <i style="color:white" class="fab fa-linkedin-in"></i>
        </div>
        
    </div>
   
        <button id="mybtn"onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Post a crime</button>
            <div id="id01" class="modal">
                <form class="modal-content animate" action="/action_page.php">
                    <div class="imgcontainer">
                        <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
                         <h5>Post a Crime</h5>  
                    </div>
                            <div class="container">
                                        <label for="name">Full Name</label>
                                            <input type="text" placeholder="Enter your  Full name" name="" required='true'>
                                        <label for="email">Email Address</label>
                                            <input type="text" placeholder="Enter Your Email Address" name="" require>
                                        <label for="crime_address">Crime Address</label>
                                            <input type="text" placeholder="Enter Crime Address" name="" required>
                                        <label for="crime_address2">crime_address2</label>
                                            <input type="text" placeholder="Enter Crime Address 2" name=""required>
                                        <label for="crime">Describe Crime</label><br/>
                                            <textarea name="" id="post_area" cols="122" rows="10"></textarea>
                                        <div class="container" style="background-color:rgba(3, 50, 66, 0.8);'">
                                        <center><button id="button" type="button"  >Submit</button>
                                        <button id="button" type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button></center>
                                            
                            </div>
                </form>
            </div>
<!-- end of post rime -->

</div>    
      <script>
        var modal=document.getElementById('post');
        window.onclick =function(event){
            if(event.target ==modal)
            {
                modal.style.display = "none";
            }
            }
        $ (function(){
            $("#menu").click(function(){
                $("#nav_link").slideToggle();
            });
            $('.lesson-hover').hover(
            function() {
            $(this).find('.text-contents').addClass('text-active').slideUp(slow);
            },
             function() {
            $(this).find('.text-contents').removeClass('text-active'); 
                     });
           });
            $(document).ready(function(){
            $('body').css('display', 'none');
            $('body').fadeIn(2000);  
       });
      </script>
      
 
</body>
</html>